package com.assessment.cartService.service.impl;

import com.assessment.cartService.entity.Cart;
import com.assessment.cartService.entity.Product;
import com.assessment.exception.ProductNotFoundException;
import com.assessment.exception.QuantityExceededException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartServiceImpl extends CrudServiceImpl<Cart>
{
	// FIXME: add ProductServiceImpl dependency
	
	@Autowired
	ProductServiceImpl productServiceImpl;

	@Override
	protected Cart update(Cart input) {
		Cart dbCart = get(input.getId());
		if(dbCart == null) {
			throw new IllegalArgumentException("Attempting to update entity that does not exist");
		}
		// this update method will replace the stored products list with the list from the input
		// any logic to merge or remove items from the cart collection should happen in business logic prior to this
		dbCart.setProducts(input.getProducts());

		return dbCart;
	}

	// FIXME: applicant should finish the implementation of this method
	public Cart submitCart(Cart cart) throws Exception {

		// FIXME:  add required logic here for cart submission

		
		if (null != cart) {
			Cart dbCart = get(cart.getId());
			
			// fetch the persisted cart from the database and merge the products lists, note that the list should allow multiple copies of an individual product
			dbCart.getProducts().addAll(cart.getProducts());
			
			for (Product cartProduct : cart.getProducts()) {
				Product dbProduct = productServiceImpl.get(cartProduct.getId());
				
				if (!exists(cartProduct.getId())) {
					throw new ProductNotFoundException("The product does not exist");
				} else {
					if(dbProduct.getQuantity() < cartProduct.getQuantity()) {
						throw new QuantityExceededException("The product exceeds the available quantity");
					}
					
					dbCart.getProducts().add(cartProduct);  // merge the product into the cart
					productServiceImpl.update(cartProduct); // and persist the results to the database
					return update(cart);
				}
			}
		} else {
			return save(cart);
		}
		
		return cart;
		
	}
}

