package com.assessment.exception;

public class QuantityExceededException extends Exception {

	public QuantityExceededException(String message) {
		super(message);
	}
}
